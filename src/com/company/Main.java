package com.company;

import java.util.Locale;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Task1();
        Task2dot1();
        Task2dot2();
    }

    public static void Task1() {
        //1. Дан список книг. Книга содержит название, автора и год издания.
        // Необходимо: найти самую старую книгу и вывести ее автора; найти книгу определенного автора и вывести ее название
        // (или названия); найти книги, изданные ранее введенного года и вывести всю информацию по ним.

        System.out.println("Task 1");
        //создаем обьекты класса Books
        Books[] books = new Books[5];

        books[0] = new Books(1719, "Robinson Crusoe", "daniel defoe");
        books[1] = new Books(1869, "War and Peace", "Leo Tolstoy");
        books[2] = new Books(1862, "The House of the Dead", "Fyodor Dostoevsky");
        books[3] = new Books(1925, "The Great Gatsby", "F Scott Fitzgerald");
        books[4] = new Books(1856, "The Snowstorm", "Leo Tolstoy");

        int minValue = Integer.MAX_VALUE;
        int arrayNumber = 0;
        //найти самую старую книгу и вывести ее автора
        for (int i = 0; i < books.length; i++) {
            if (books[i].createdDate < minValue) {
                minValue = books[i].createdDate;
                arrayNumber = i;
            }
        }
        System.out.println("The name of the author of the oldest book in list is: " + books[arrayNumber].author.toUpperCase());
        //найти книгу определенного автора и вывести ее название
        //(или названия);
        String keyAuthor = "Leo Tolstoy";
        for (int i = 0; i < books.length; i++) {
            if (books[i].author.toLowerCase().equals(keyAuthor.toLowerCase())) {
                System.out.println(String.format("книги автора %s :", keyAuthor.toUpperCase()) + books[i].nameOfTheBook.toUpperCase());
            }
        }

        // найти книги, изданные ранее введенного года и вывести всю информацию по ним.
        Scanner scanner = new Scanner(System.in);
        System.out.println(String.format("Ранее какого года вы хотите увидеть (%d минимальный год) перечень книг?", minValue));
        int minYear = Integer.parseInt(scanner.nextLine());
        for (int i = 0; i < books.length; i++) {
            if (books[i].createdDate < minYear) {
                System.out.println(books[i].author + " : " + books[i].nameOfTheBook + " // " + books[i].createdDate);
            }
        }

    }

    public static void Task2dot1() {
        //2. Дана программа для конвертации валют по введенному курсу (JavaExampleI.java).
        //2.1. На ее основе необходимо сделать программу для конвертации гривны в доллар по одному
        //из заранее известных (значения задаются в переменных/константах/моделях) курсов (например,
        //курс ПриватБанка, ОщадБанка, и банка ПУМБ). Название банка вводится с консоли.

        System.out.println();
        System.out.println("Task 2.1");

        //init variables

        String USD = "USD";

        Courses privatCourses = new Courses();
        privatCourses.usdCourse = 24.5f;
        privatCourses.bankName = "privatbank";

        Courses pumbCourses = new Courses();
        pumbCourses.usdCourse = 30.5f;
        pumbCourses.bankName = "pumb";

        Courses oschadCourses = new Courses();
        oschadCourses.usdCourse = 44.5f;
        oschadCourses.bankName = "oschadbank";

        //init scanner
        Scanner scan = new Scanner(System.in);

        //enter amount of money
        System.out.println("Enter the amount of money that you want to change to USD: ");
        int amount = Integer.parseInt(scan.nextLine());

        //enter bank
        System.out.println("Enter from which bank do you want to convert your grivnas (you can use only PUMB or PRIVATBANK or OSHADBANK): ");
        String bank = scan.nextLine();


        //convert uah to USD currency
        if (oschadCourses.bankName.equalsIgnoreCase(bank)) {
            System.out.println(String.format(Locale.US, "You money in %s: %.2f", USD, amount / oschadCourses.usdCourse));
        } else if (privatCourses.bankName.equalsIgnoreCase(bank)) {
            System.out.println(String.format(Locale.US, "You money in %s: %.2f", USD, amount / privatCourses.usdCourse));
        } else if (pumbCourses.bankName.equalsIgnoreCase(bank)) {
            System.out.println(String.format(Locale.US, "You money in %s: %.2f", USD, amount / pumbCourses.usdCourse));
        } else {
            System.err.println("Can't convert to USD");
        }
    }

    public static void Task2dot2() {
        //2. Дана программа для конвертации валют по введенному курсу (JavaExampleI.java).
        //2.1. На ее основе необходимо сделать программу для конвертации гривны в доллар по одному
        //из заранее известных (значения задаются в переменных/константах/моделях) курсов
        //(например, курс ПриватБанка, ОщадБанка, и банка ПУМБ). Название банка вводится с консоли.
        System.out.println();
        System.out.println("Task 2.2");

        //init variables

        String USD = "USD";
        String EUR = "EUR";
        String RUB = "RUB";

        Courses privat = new Courses();
        privat.eurCourse = 30.01f;
        privat.rubCourse = 0.41f;
        privat.usdCourse = 24.5f;
        privat.bankName = "privatbank";

        Courses pumb = new Courses();
        pumb.rubCourse = 0.42f;
        pumb.eurCourse = 31f;
        pumb.usdCourse = 27.5f;
        pumb.bankName = "pumb";

        Courses oschad = new Courses();
        oschad.usdCourse = 26.5f;
        oschad.eurCourse = 30f;
        oschad.rubCourse = 0.43f;
        oschad.bankName = "oschadbank";

        //init scanner
        Scanner scan = new Scanner(System.in);

        //enter amount of money
        System.out.println("Enter the amount of money that you want to change to USD: ");
        int amount = Integer.parseInt(scan.nextLine());

        //enter bank
        System.out.println("Enter from which bank do you want to convert your grivnas (you can use only PUMB or PRIVATBANK or OSHADBANK): ");
        String bank = scan.nextLine();

        //enter currency
        System.out.println("Enter to what kind of currency do you want to change your grivnas (you can use only EUR or USD or RUB): ");
        String currency = scan.nextLine();

        //convert uah to preferred currency
        for (int i = 0; i < 3; i++) {

        }
        if (oschad.bankName.equalsIgnoreCase(bank)) {
            if (currency.equalsIgnoreCase(EUR)) {
                System.out.println(String.format(Locale.US, "You money in %s: %.2f", EUR, amount / oschad.eurCourse));
            } else if (currency.equalsIgnoreCase(USD)) {
                System.out.println(String.format(Locale.US, "You money in %s: %.2f", USD, amount / oschad.usdCourse));
            } else if (currency.equalsIgnoreCase(RUB)) {
                System.out.println(String.format(Locale.US, "You money in %s: %.2f", RUB, amount / oschad.rubCourse));
            } else {
                System.err.println("Sorry for inconvenience. Please try again later :)");
            }
        } else if (privat.bankName.equalsIgnoreCase(bank)) {
            if (currency.equalsIgnoreCase(EUR)) {
                System.out.println(String.format(Locale.US, "You money in %s: %.2f", EUR, amount / privat.eurCourse));
            } else if (currency.equalsIgnoreCase(USD)) {
                System.out.println(String.format(Locale.US, "You money in %s: %.2f", USD, amount / privat.usdCourse));
            } else if (currency.equalsIgnoreCase(RUB)) {
                System.out.println(String.format(Locale.US, "You money in %s: %.2f", RUB, amount / privat.rubCourse));
            } else {
                System.err.println("Sorry for inconvenience. Please try again later :)");
            }
        } else if (pumb.bankName.equalsIgnoreCase(bank)) {
            if (currency.equalsIgnoreCase(EUR)) {
                System.out.println(String.format(Locale.US, "You money in %s: %.2f", EUR, amount / pumb.eurCourse));
            } else if (currency.equalsIgnoreCase(USD)) {
                System.out.println(String.format(Locale.US, "You money in %s: %.2f", USD, amount / pumb.usdCourse));
            } else if (currency.equalsIgnoreCase(RUB)) {
                System.out.println(String.format(Locale.US, "You money in %s: %.2f", RUB, amount / pumb.rubCourse));
            } else {
                System.err.println("Sorry for inconvenience. Please try again later :)");
            }
        } else {
            System.err.println("You did smth wrong, please do not use our system anymore, you are poor client :)");
        }
    }
}
