package com.company;

import java.awt.print.Book;

public class Books {
    public String nameOfTheBook = new String();
    public String author = new String();
    public int createdDate;

    public void setNameOfTheBook(String nameOfTheBook) {
        this.nameOfTheBook = nameOfTheBook;
    }
    public Books(){

    }
    public Books(int createdDate, String nameOfTheBook, String author){
        this.createdDate = createdDate;
        this.nameOfTheBook = nameOfTheBook;
        this.author = author;
    }
    public String getNameOfTheBook() {
        return nameOfTheBook;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(int createdDate) {
        this.createdDate = createdDate;
    }

}
